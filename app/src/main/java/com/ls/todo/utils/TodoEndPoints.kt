package com.ls.todo.utils

/**
 * Author : Shrikant Devarmani  <shrikantdevarmani228@gmail.com>
 * Created On : 04, December, 2019
 * Company : WorkingOnIt company, Maharashtra-India.
 *
 * Description : 04, December, 2019
 * Modified On : Task end points
 */

object TodoEndPoints {

    fun getTask(): String {
        return StaticHelper.TODO_BASE_URL + StaticHelper.TODOS
    }
}
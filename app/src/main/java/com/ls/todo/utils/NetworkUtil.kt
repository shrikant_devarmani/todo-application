package com.ls.todo.utils

import android.content.Context
import android.net.ConnectivityManager


/**
 * Author : Shrikant Devarmani  <shrikantdevarmani228@gmail.com>
 * Created On : 05, December, 2019
 * Company : WorkingOnIt company, Maharashtra-India.
 *
 * Description : 05, December, 2019
 * Modified On : internet connection check
 */
class NetworkUtil {
    companion object {
        fun getConnectivityStatus(context: Context): String {
            var status: String? = null
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            if (activeNetwork != null) {
                if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                    status = "W"
                    return status
                } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                    status = "M"
                    return status
                }
            } else {
                status = "N"
                return status
            }
            return status!!
        }
    }
}
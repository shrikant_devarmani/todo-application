package com.ls.todo.utils

/**
 * Author : Shrikant Devarmani  <shrikantdevarmani228@gmail.com>
 * Created On : 04, December, 2019
 * Company : WorkingOnIt company, Maharashtra-India.
 *
 * Description : static helper for application level context
 * Modified On : 04, December, 2019
 */

object StaticHelper {
    var TODO_BASE_URL = "https://jsonplaceholder.typicode.com/"
    var TODOS = "todos"
    var TITLE = "title"
    var STATUS = "completed"
    var ALL = "all"
    var COMPLETED = 1
    var UNCIMPLETED = 0

}
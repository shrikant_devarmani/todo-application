package com.ls.todo.interfaces

import com.ls.todo.model.Task

/**
 * Author : Shrikant Devarmani  <shrikantdevarmani228@gmail.com>
 * Created On : 04, December, 2019
 * Company : WorkingOnIt company, Maharashtra-India.
 *
 * Description : events related to task items
 * Modified On :04, December, 2019
 */

interface TaskListener {
    fun deleteTask(task: Task, position: Int)
    fun editTask(task: Task, position: Int)
    fun setCompletedStatus(task: Task, position: Int)
    fun showNoDataView(showHide :Boolean)
}
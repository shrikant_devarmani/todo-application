package com.ls.todo.model

import java.io.Serializable

/**
 * Author : Shrikant Devarmani  <shrikantdevarmani228@gmail.com>
 * Created On : 04, December, 2019
 * Company : WorkingOnIt company, Maharashtra-India.
 *
 * Description : model class for user task
 * Modified On : 04, December, 2019
 */

class Task() : Serializable {
    var userId: Int = 0
    var id: Int = 0
    var title: String = ""
    var completed: Int = 0
}
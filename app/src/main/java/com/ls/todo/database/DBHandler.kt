package com.ls.todo.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.ls.todo.model.Task

/**
 * Author : Shrikant Devarmani  <shrikantdevarmani228@gmail.com>
 * Created On : 04, December, 2019
 * Company : WorkingOnIt company, Maharashtra-India.
 *
 * Description : Database for todo application
 * Modified On :04, December, 2019
 */
class DBHandler(
    context: Context,
    factory: SQLiteDatabase.CursorFactory?
) :
    SQLiteOpenHelper(
        context, DATABASE_NAME,
        factory, DATABASE_VERSION
    ) {
    override fun onCreate(db: SQLiteDatabase) {
        val todoTable = ("CREATE TABLE " + TABLE_NAME + "("
                + TASK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + ID + " INTEGER,"
                + TASK_TITLE + " TEXT,"
                + STATUS + " INTEGER DEFAULT 0)")
        db.execSQL(todoTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    /**
     * db related static values
     */
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "tasktodo.db"
        val TABLE_NAME = "todo_table"
        val ID = "id"
        val TASK_ID = "task_id"
        val TASK_TITLE = "TITLE"
        val STATUS = "completed"
    }

    /**
     * add task to db
     */
    fun addTask(task: Task): Task? {
        val values = ContentValues()
        values.put(ID, task.id)
        values.put(TASK_TITLE, task.title)
        values.put(STATUS, task.completed)

        val db = this.writableDatabase
        val inserted = db.insert(TABLE_NAME, null, values)

        val insertedTask = getTaskByInsertedId(inserted)
        db.close()

        return insertedTask!!
    }

    /**
     * get lastly inserted task with task id
     */
    private fun getTaskByInsertedId(insertedId: Long): Task? {
        val db = this.readableDatabase
        val cursor = db.rawQuery("SELECT * FROM $TABLE_NAME WHERE TASK_ID =$insertedId", null)
        cursor.moveToFirst()
        if (cursor.count <= 0) {
            return null
        }
        return taskCreation(cursor)
    }

    /**
     * Delete task with task id
     */
    fun deleteTask(taskId: Int): Int {
        val db = this.writableDatabase
        val deleted = db.delete(TABLE_NAME, "$TASK_ID= ? ", arrayOf(taskId.toString()))
        db.close()

        return deleted
    }

    /**
     * Update task depends upon task_id
     */
    fun updateTaskName(task: Task): Int {
        val values = ContentValues()
        values.put(TASK_TITLE, task.title)

        val db = this.writableDatabase
        val updated = db.update(TABLE_NAME, values, "$TASK_ID=${task.userId}", null)
        db.close()

        return updated
    }

    /**
     * Mark task complete/uncomplete
     */
    fun taskStatusMark(task: Task): Int {
        val values = ContentValues()
        values.put(STATUS, task.completed)

        val db = this.writableDatabase
        val updated = db.update(TABLE_NAME, values, "$TASK_ID=${task.userId}", null)
        db.close()

        return updated
    }

    /**
     * Reusable task entity formation
     */
    private fun taskCreation(cursor: Cursor): Task {
        val task = Task()
        task.id = cursor.getString(cursor.getColumnIndex(ID)).toInt()
        task.userId = cursor.getString(cursor.getColumnIndex(TASK_ID)).toInt()
        task.title = cursor.getString(cursor.getColumnIndex(TASK_TITLE))
        task.completed = cursor.getString(cursor.getColumnIndex(STATUS)).toInt()
        return task
    }
}
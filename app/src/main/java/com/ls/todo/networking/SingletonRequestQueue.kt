package com.ls.todo.networking

import android.annotation.SuppressLint
import android.content.Context
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley


/**
 * Author : Shrikant Devarmani  <shrikantdevarmani228@gmail.com>
 * Created On : 04, December, 2019
 * Company : WorkingOnIt company, Maharashtra-India.
 *
 * Description : volley singleton class
 * Modified On :04, December, 2019
 */

class SingletonRequestQueue(var context: Context) {
    private var mRequestQueue: RequestQueue? = null

    init {
        mRequestQueue = getRequestQueue()
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        private var mInstance: SingletonRequestQueue? = null

        fun getInstance(context: Context): SingletonRequestQueue {
            if (mInstance == null) {
                mInstance = SingletonRequestQueue(context)
            }
            return mInstance as SingletonRequestQueue
        }
    }

    fun getRequestQueue(): RequestQueue? {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context)
        }
        return mRequestQueue
    }
}
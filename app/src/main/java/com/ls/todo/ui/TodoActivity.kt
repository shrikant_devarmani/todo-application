package com.ls.todo.ui

import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.ls.todo.R
import com.ls.todo.adapters.TodoAdapter
import com.ls.todo.database.DBHandler
import com.ls.todo.databinding.ActivityMainBinding
import com.ls.todo.interfaces.TaskListener
import com.ls.todo.model.Task
import com.ls.todo.networking.SingletonRequestQueue
import com.ls.todo.utils.NetworkUtil
import com.ls.todo.utils.StaticHelper
import com.ls.todo.utils.TodoEndPoints
import java.util.*


/**
 * Author : Shrikant Devarmani  <shrikantdevarmani228@gmail.com>
 * Created On : 04, December, 2019
 * Company : WorkingOnIt company, Maharashtra-India.
 *
 * Description : TODOActivity is home ui for task
 * Modified On : 04, December, 2019
 *               05, December, 2019 [no data view filter changes according to complete and incomplete]
 */


class TodoActivity : AppCompatActivity(), View.OnClickListener, TaskListener,
    RadioGroup.OnCheckedChangeListener {

    private lateinit var mBinding: ActivityMainBinding
    private var mAdapter: TodoAdapter? = null
    private var mTaskList: ArrayList<Task>? = null
    private var dbHandler: DBHandler? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mBinding.executePendingBindings()

        // init components and variable
        initComponent()

        if (!NetworkUtil.getConnectivityStatus(this@TodoActivity).equals("N", true)) {
            // create volley request
            createVolleyRequest()
        } else {
            mBinding.progressBar.visibility = View.GONE
            mBinding.rvTodo.visibility = View.VISIBLE
            toastMessage(resources.getString(R.string.lbl_no_internet))
        }
    }

    /**
     * Recycler view init
     */
    private fun initRecyclerView() {
        /**
         * view visibility changes
         */
        mBinding.progressBar.visibility = View.GONE
        mBinding.rvTodo.visibility = View.VISIBLE


        mAdapter = TodoAdapter(this@TodoActivity, this)
        mBinding.rvTodo.adapter = mAdapter
        mTaskList?.let { mAdapter!!.updateList(it) }
    }

    /**
     * Init initial data/component
     */
    private fun initComponent() {
        mBinding.btnAddTask.setOnClickListener(this)
        mBinding.rbGroup.setOnCheckedChangeListener(this)

        mTaskList = ArrayList()
        dbHandler = DBHandler(this, null)
    }


    /**
     * volley request for json array
     */
    private fun createVolleyRequest() {

        try {

            // getting request queue instance
            val requestQueue: RequestQueue =
                SingletonRequestQueue.getInstance(applicationContext).getRequestQueue()!!

            //creating Json Array Request
            val todoArrayRequest = JsonArrayRequest(
                TodoEndPoints.getTask(),
                Response.Listener {

                    // for dynamic commented line work
                    // for (i in 0 until it.length()) {

                    /**
                     * only first 5 items in list of json
                     */
                    for (i in 0 until 5) {
                        val task = Task()

                        task.id = i
                        task.userId = i + 1
                        task.title = it.getJSONObject(i).getString(StaticHelper.TITLE)
                        task.completed =
                            if (!it.getJSONObject(i).getBoolean(StaticHelper.STATUS)) 0 else 1

                        mTaskList!!.add(dbHandler!!.addTask(task)!!)
                    }

                    // init recycler view
                    initRecyclerView()
                    itemCount()

                }, Response.ErrorListener {
                    // todo handle error response of api call
                    mBinding.progressBar.visibility = View.GONE
                    toastMessage(resources.getString(R.string.lbl_data_fetch_error))
                })

            requestQueue.add(todoArrayRequest)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * On view click callback
     */
    override fun onClick(v: View?) {
        when (v!!.id) {
            mBinding.btnAddTask.id -> {

                // empty validation
                if (mBinding.edtTaskName.text.isEmpty()) {
                    toastMessage(resources.getString(R.string.lbl_task_name_empty))
                    return
                }

                // create task entity
                val task = Task()
                task.id = mTaskList!!.size + 1
                task.title = mBinding.edtTaskName.text.toString()
                task.completed = 0


                //  add to db and get returned with created id
                val addedTask = dbHandler!!.addTask(task)

                // update adapter list and notify related changes like count, view etc
                if (addedTask != null) {
                    toastMessage(mBinding.edtTaskName.text.toString() + " added successfully.")
                    mBinding.edtTaskName.text.clear()

                    if (mAdapter != null) {
                        mAdapter!!.updateList(addedTask)
                    } else {
                        mTaskList!!.add(addedTask)
                        initRecyclerView()
                    }
                    //set number of task's in list count at action bar
                    itemCount()

                    // filter applied for added task when completed filter is selected
                    applyFilterOnAddedTak(task, mAdapter!!.filterableTaskList.size - 1)
                }
            }
        }
    }


    /**
     * Item count of todo's in action bar shown
     */
    private fun itemCount() {
        try {
            supportActionBar!!.title =
                resources.getString(R.string.app_name) + " ( ${mAdapter!!.filterableTaskList.size}  Items )"

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    /**
     * Radio group chcck change listener
     */
    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            mBinding.rbAll.id -> {
                mAdapter!!.filter.filter(StaticHelper.ALL)
            }
            mBinding.rbCompleted.id -> {
                mAdapter!!.filter.filter(StaticHelper.COMPLETED.toString())
            }
            mBinding.rbUncompleted.id -> {
                mAdapter!!.filter.filter(StaticHelper.UNCIMPLETED.toString())

            }
        }
    }

    /**
     * delete task callback
     */
    override fun deleteTask(task: Task, position: Int) {
        if (dbHandler!!.deleteTask(task.userId) > 0) {
            mAdapter!!.removeItemFromList(task)
            toastMessage("Deleted task successfully.")
            itemCount()
        } else {
            toastMessage("Failed to delete task.")
        }
    }

    /**
     * Toast message method
     */
    private fun toastMessage(message: String) {
        Toast.makeText(
            this,
            message,
            Toast.LENGTH_LONG
        ).show()
    }

    /**
     * Edit task callback
     */
    override fun editTask(task: Task, position: Int) {

        if (dbHandler!!.updateTaskName(task) > 0) {
            mAdapter!!.updateItemFromList(task, position)
            toastMessage("Update task successfully.")
        } else {
            toastMessage("Failed to update task.")
        }
    }

    /**
     * task completed and incomplete
     */
    override fun setCompletedStatus(task: Task, position: Int) {
        dbHandler!!.taskStatusMark(task)

        //Filtering the completed and in complete
        applyFilterOnAddedTak(task, position)
    }

    /**
     * Show and hide the no data view
     */
    override fun showNoDataView(showHide: Boolean) {
        if (showHide) {
            mBinding.tvNoToItems.visibility = View.VISIBLE
        } else {
            mBinding.tvNoToItems.visibility = View.GONE
        }
    }

    /**
     * Filter for applied for added task
     */
    private fun applyFilterOnAddedTak(task: Task, position: Int) {
        try {
            if (mBinding.rbGroup.checkedRadioButtonId != mBinding.rbAll.id) {
                if (task.completed == 1) {
                    mAdapter!!.filter.filter(StaticHelper.UNCIMPLETED.toString())
                } else {
                    mAdapter!!.filter.filter(StaticHelper.COMPLETED.toString())
                }
            } else {
//            mAdapter?.notifyItemChanged(position)
                mAdapter?.notifyDataSetChanged()
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}

package com.ls.todo.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ls.todo.R
import com.ls.todo.databinding.CardTodoNameItemBinding
import com.ls.todo.interfaces.TaskListener
import com.ls.todo.model.Task
import com.ls.todo.utils.StaticHelper


/**
 * Author : Shrikant Devarmani  <shrikantdevarmani228@gmail.com>
 * Created On : 04, December, 2019
 * Company : WorkingOnIt company, Maharashtra-India.
 *
 * Description : todolist adapter
 * Modified On :04, December, 2019
 */
class TodoAdapter(var context: Context, var listener: TaskListener) :
    RecyclerView.Adapter<TodoAdapter.TaskHolder>(), Filterable {

    //original list
    var taskList: ArrayList<Task> = ArrayList()
    //change list according to filter and curd operation
    var filterableTaskList: ArrayList<Task> = ArrayList()

    /**
     * inflate layout
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskHolder {
        val binding = DataBindingUtil.inflate<CardTodoNameItemBinding>(
            LayoutInflater.from(context),
            R.layout.card_todo_name_item,
            parent,
            false
        )
        return TaskHolder(binding)
    }

    /**
     * returns the list size
     */
    override fun getItemCount(): Int {
        return filterableTaskList.size
    }

    /**
     * binds the data to viewholder
     */
    override fun onBindViewHolder(holder: TaskHolder, position: Int) {
        holder.binding.taskModel = filterableTaskList[position]
    }

    /**
     * To update list data
     */
    fun updateList(list: List<Task>) {
        taskList.addAll(list)
        filterableTaskList.addAll(list)
        notifyDataSetChanged()
    }

    /**
     * add single entity to list
     */
    fun updateList(task: Task) {
        taskList.add(task)
        filterableTaskList.add(task)
        noDataView()
        notifyDataSetChanged()
    }

    /**
     * Clear list
     */
    fun removeItemFromList(task: Task) {
        taskList.remove(task)
        filterableTaskList.remove(task)

        noDataView()
        notifyDataSetChanged()
    }

    /**
     * update list
     */
    fun updateItemFromList(task: Task, position: Int) {
        // removing before adding into list
        taskList.removeAt(position)
        filterableTaskList.removeAt(position)

        // adding into position
        taskList.add(position, task)
        filterableTaskList.add(position, task)
        notifyDataSetChanged()
    }

    /**
     * task holder class
     */
    inner class TaskHolder(var binding: CardTodoNameItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.tvItemOptions.setOnClickListener {
                //creating a popup menu
                val popup = PopupMenu(context, binding.tvItemOptions)

                //inflating menu from xml resource
                popup.inflate(R.menu.task_option_menu)

                //adding click listener
                popup.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.actionUpdate -> {
                            if (filterableTaskList[adapterPosition].completed == 1) {
                                toastMessage("Task already completed.")
                            } else {
                                // get enabled for edit
                                binding.imgSaveEditedTask.visibility = View.VISIBLE
                                binding.tvTodoName.isEnabled = true
                            }
                        }

                        R.id.actionDelete -> {
                            // trigger delete event
                            listener.deleteTask(
                                filterableTaskList[adapterPosition],
                                adapterPosition
                            )
                        }
                    }
                    false
                }

                //displaying the popup
                popup.show()
            }


            /**
             * Edit action
             */
            binding.imgSaveEditedTask.setOnClickListener {
                //set the data to title
                filterableTaskList[adapterPosition].title = binding.tvTodoName.text.toString()

                // trigger edit event
                listener.editTask(filterableTaskList[adapterPosition], adapterPosition)
                binding.tvTodoName.isEnabled = false
                binding.imgSaveEditedTask.visibility = View.GONE
            }

            /**
             * Complete and uncomplete
             */
            binding.rbStatus.setOnClickListener {
                if (filterableTaskList[adapterPosition].completed == 1) {
                    filterableTaskList[adapterPosition].completed = 0
                    listener.setCompletedStatus(
                        filterableTaskList[adapterPosition],
                        adapterPosition
                    )
                } else {
                    filterableTaskList[adapterPosition].completed = 1
                    listener.setCompletedStatus(
                        filterableTaskList[adapterPosition],
                        adapterPosition
                    )
                }
                //notifyItemChanged(adapterPosition)
            }
        }
    }


    /**
     * Filterable of data for all , Completed, incompleted
     */

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                when {
                    //for all
                    charString == StaticHelper.ALL -> {
                        filterableTaskList.clear()
                        filterableTaskList.addAll(taskList)
                    }

                    //for completed
                    charString.toInt() == StaticHelper.COMPLETED -> {
                        val filteredList = ArrayList<Task>()

                        for (task in taskList) {
                            if (task.completed == 1) {
                                filteredList.add(task)
                            }
                        }

                        filterableTaskList = filteredList
                    }

                    //for un complete
                    charString.toInt() == StaticHelper.UNCIMPLETED -> {
                        val filteredList = ArrayList<Task>()

                        for (task in taskList) {
                            if (task.completed == 0) {
                                filteredList.add(task)
                            }
                        }

                        filterableTaskList = filteredList
                    }
                }

                val filterResults = FilterResults()
                filterResults.values = filterableTaskList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filterableTaskList = filterResults.values as ArrayList<Task>

                // refresh the list with filtered data
                notifyDataSetChanged()

                noDataView()
            }
        }
    }

    /**
     * No data view when list is empty
     */
    private fun noDataView() {
        if (filterableTaskList.isEmpty()) {
            listener.showNoDataView(true)
        } else {
            listener.showNoDataView(false)
        }
    }

    /**
     * Toast message method
     */
    private fun toastMessage(message: String) {
        Toast.makeText(
            context,
            message,
            Toast.LENGTH_LONG
        ).show()
    }
}